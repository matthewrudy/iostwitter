//
//  RDYViewController.h
//  PragProgTwitter
//
//  Created by Matthew Rudy Jacobs on 05/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDYViewController : UIViewController

- (IBAction)handleTweetThisButtonTapped:(id) sender;
- (IBAction)handleMyTweetsButtonTapped:(id) sender;
- (IBAction)handleSomethingRandomButtonTapped:(id) sender;

@end
