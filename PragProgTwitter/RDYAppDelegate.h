//
//  RDYAppDelegate.h
//  PragProgTwitter
//
//  Created by Matthew Rudy Jacobs on 05/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
