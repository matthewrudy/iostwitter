//
//  RDYViewController.m
//  PragProgTwitter
//
//  Created by Matthew Rudy Jacobs on 05/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RDYViewController.h"
#import "Social/Social.h"

@interface RDYViewController ()

-(void) reloadTweets;
@property (nonatomic, strong) IBOutlet UITextView *twitterTextView;

@end

@implementation RDYViewController

-(IBAction) handleTweetThisButtonTapped:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetvc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetvc setInitialText:NSLocalizedString(@"Yo, I'm sending a tweet", NULL)];
        
        tweetvc.completionHandler = ^(SLComposeViewControllerResult result) {
            [self dismissViewControllerAnimated:YES completion:NULL];
            if (result == SLComposeViewControllerResultDone) {
                NSLog(@"tweeting succeeded");
                [self reloadTweets];
            } else {
                NSLog(@"tweeting failed");
            }
        };
        [self presentViewController:tweetvc animated:TRUE completion:NULL];
    }
}

-(IBAction) handleMyTweetsButtonTapped:(id)sender
{
    [self reloadTweets];
}

-(IBAction) handleSomethingRandomButtonTapped:(id)sender
{
    NSLog(@"something random just happened");
}

-(void) reloadTweets
{
    NSString *username = @"matthewrudy";
    
    NSURL *timelineURL = [NSURL URLWithString: @"http://api.twitter.com/1/statuses/user_timeline.json"];
    NSDictionary *params = @{@"screen_name": username};
    
    SLRequest *twitterRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:timelineURL parameters:params];
    
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSArray *tweets = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setTwitterText:tweets];
        });
    }];

}

-(void) setTwitterText:(NSArray *)tweets
{
    self.twitterTextView.text = [self buildTwitterText:tweets];
}

-(NSString *) buildTwitterText:(NSArray *)tweets
{
    NSString *text = @"";
    NSString *separator = @"\n\n";
    NSString *tweetText;
    
    NSArray *sortedTweets = [self sortTweets: tweets];
    
    for(NSDictionary *tweet in sortedTweets) {
        tweetText = [NSString stringWithFormat: @"%@: %@ (%@)",
            [tweet valueForKeyPath:@"user.name"],
            [tweet valueForKey: @"text"],
            [tweet valueForKey: @"created_at"]
        ];
                     
        text = [NSString stringWithFormat: @"%@%@%@", text, tweetText, separator];
    }
    
    return text;
}

-(NSArray *) sortTweets:(NSArray *) tweets
{
    return [tweets sortedArrayWithOptions:NSSortConcurrent usingComparator: ^NSComparisonResult(id obj1, id obj2) {
        NSString *date1 = [obj1 valueForKey: @"created_at"];
        NSString *date2 = [obj2 valueForKey: @"created_at"];
        
        if (date1 == date2) {
            NSString *name1 = [obj1 valueForKeyPath: @"user.name"];
            NSString *name2 = [obj2 valueForKeyPath: @"user.name"];
            return [name1 compare: name2];
        } else {
            return [date1 compare: date2];
        }
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
